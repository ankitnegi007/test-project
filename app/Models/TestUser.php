<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestUser extends Model
{
    use HasFactory;

    protected $table = 'test_users';
    
    protected $fillable = ['user_name', 'user_email', 'user_password', 'user_type'];
    public $timestamps = false;
}
