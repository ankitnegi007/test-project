<?php

namespace App\Exports;

use App\Models\Data;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;


class ConsumerDataExport implements FromCollection, WithHeadings, WithStyles, WithColumnWidths, WithMapping
{
    // public function collection()
    // {
    //     return Data::all();
    // }
    use Exportable;

    protected $columns;
    private $serialNumber = 0;


    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    public function collection()
    {
        return Data::select($this->columns)->get();
    }

    public function headings(): array
    {
        return [
            'S.No.',
            'Name',
            'Gender',
            'Email',
            'Phone',
            'State',
        ];
    }

    public function map($row): array
    {
        return [
            ++$this->serialNumber,
            $row->name,
            $row->gender,
            $row->email,
            $row->phone,
            $row->state,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        // $sheet->getStyle('A1:E1')->getFont()->setBold(true);

        $sheet->getStyle('A1:F1')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'ADD8E6',
                ],
            ],
        ]);

        $sheet->getStyle($sheet->calculateWorksheetDimension())->getAlignment()->setHorizontal('left');
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 20,
            'C' => 10,
            'D' => 20,
            'E' => 15,
            'F' => 20
        ];
    }
}
