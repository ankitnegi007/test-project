<?php

namespace APP\Models;

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\TestUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exports\ConsumerDataExport;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;


class UserRegistration extends Controller
{
    function registerUser(Request $req) {
        $req->validate([
            'user_name' => 'required|string',
            'user_email' => 'required|email',
            'user_type' => 'required',
            'user_password' => 'required|confirmed',
        ]);

        $test_user = new TestUser();
        $test_user->user_name = $req->user_name;
        $test_user->user_email = $req->user_email;
        $test_user->user_type = $req->user_type;
        $test_user->user_password = Hash::make($req->user_password);
        $test_user->save();

        return redirect('login')->with('success', 'Your account has been created successfully!');
    }


    public function addConsumer(Request $req)
    {
        $req->validate([
            'name' => 'required|string',
            'gender' => 'required|in:M,F',
            'email' => 'required|email',
            'phone' => 'required',
            'state' => 'required',
        ]);

        $consumer = new Data();
        $consumer->name = $req->name;
        $consumer->gender = $req->gender;
        $consumer->email = $req->email;
        $consumer->phone = $req->phone;
        $consumer->state = $req->state;
        $consumer->save();

        return redirect('/consumers')->with('success', 'Form submitted successfully!');
    }

    function showConsumers()
    {
        $consumers = DB::table('users')->orderBy('created_at')->get();

        return view('allConsumers', ['data' => $consumers]);
    }

    function deleteConsumer(string $id)
    {
        $consumer = DB::table('users')->where('id', $id)->delete();

        if ($consumer) {
            return redirect()->route('addConsumer')->with('delete', 'Consumer successfully deleted');
        } else {
            echo "<h5>Data not deleted.</h5>";
        }
    }

    public $currentDate;

    public function __construct()
    {
        $this->currentDate = Carbon::now()->format('d-M-Y');
    }

    public function exportExcel()
    {
        $fileName = 'consumers_' . $this->currentDate . '.xlsx';
        $columns = ['name', 'gender', 'email', 'phone', 'state'];
        return Excel::download(new ConsumerDataExport($columns), $fileName);
    }

    public function exportPdf()
    {
        $fileName = 'consumers_' . $this->currentDate . '.pdf';
        $columns = ['name', 'gender', 'email', 'phone', 'state'];

        return Excel::download(new ConsumerDataExport($columns), $fileName);
    }
}
