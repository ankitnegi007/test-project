<?php

namespace App\Http\Controllers;

use App\Models\TestUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        if (session()->has('user_id')) {
            return redirect()->route('addConsumer');
        }
    
        return view('login');
    }

    public function loginUser(Request $req)
    {
        $req->validate([
            'user_email' => 'required|email',
            'user_password' => 'required',
        ]);


        $user = TestUser::where('user_email', $req->user_email)->first();

        if ($user && Hash::check($req->user_password, $user->user_password)) {
            session()->put('user_id', $user->user_id);
            session()->put('user_type', $user->user_type);

            return redirect('/consumers');
        } else {
            return redirect('/login')->with('error', 'Email/Password is invalid.');
        }
    }

    public function logout(Request $req)
    {
        // Clear all session data
        $req->session()->flush();

        // Redirect to the login page after logout
        return redirect()->route('login');
    }
}
