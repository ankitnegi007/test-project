<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = collect(
            [
                [
                    'name' => 'Ankit Singh Negi',
                    'class' => 'MCA 1st'
                ],
                [
                    'name' => 'Rajesh Kumar',
                    'class' => 'MCA 2nd'
                ],
                [
                    'name' => 'Rahul Negi',
                    'class' => 'MCA 3rd'
                ],
                [
                    'name' => 'Roshan Gupta',
                    'class' => 'MCA 1st'
                ]
            ]
        );

        $students->each(function ($student) {
            student::insert($student);
        });

        // student::create([
        //     'name'=>'Ankit Singh Negi',
        //     'class'=>'MCA'
        // ]);
    }
}
