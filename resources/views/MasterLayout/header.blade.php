<div class="header d-flex" id="list">
    <div class="icons col-sm-3 my-auto">
        @if (Session::has('user_type') && Session::get('user_type') === 'S')
            <a href="{{ URL::to('/') }}" class="home p-2">Supplier</a>
            <a href="{{ URL::to('/') }}/consumerForm" class="about p-2">Form</a>
            <a href="{{ URL::to('/') }}/consumers" class="contact p-2">All Consumers</a>
        @elseif (Session::has('user_type') && Session::get('user_type') === 'C')
            <a href="{{ URL::to('/') }}" class="home p-2">Consumer</a>
            <a href="{{ URL::to('/') }}/consumerForm" class="about p-2">Form</a>
            <a href="{{ URL::to('/') }}/consumers" class="contact p-2">Users</a>
        @else
            <a href="{{ URL::to('/') }}" class="home p-2">Home</a>
            <a href="{{ URL::to('/') }}/register" class="about p-2">Sign Up</a>
            <a href="{{ URL::to('/') }}/login" class="contact p-2">Login</a>
        @endif
    </div>
    <div class="logo col-sm-6 text-center">
        <img src="{{ URL::to('/') }}/images/cruxbytes_logo.svg" alt="">
    </div>
    @if (Session::has('user_type') && Session::get('user_type') === 'S' || Session::get('user_type') === 'C')
    <a href="{{ URL::to('/') }}/logout" class="icons col-sm-3 text-end my-auto">Log Out</a>
    @else
    <div class="col-sm-3 text-end my-auto">
        <a class="icons p-2" style="color: darkblue"><i class="fa-brands fa-facebook"></i></a>
        <a class="icons p-2" style="color: black"><i class="fa-brands fa-x-twitter"></i></a>
        <a class="icons p-2" style="color: blue"><i class="fa-brands fa-linkedin"></i></a>
    </div>
    @endif
</div>
</div>
