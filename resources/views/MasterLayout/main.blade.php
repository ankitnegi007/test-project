<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://kit.fontawesome.com/c53d88c76d.js" crossorigin="anonymous"></script>
    <link type="image/x-icon" rel="shortcut icon" href="{{ URL::to('/') }}/favicon.ico">
    <title>@yield('title')</title>

    <style>
        body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
            display: flex;
            flex-direction: column;
        }

        a {
            text-decoration: none;
            color: gray;
            font-weight: bold
        }

        i {
            font-size: 20px
        }

        #list {
            background: #D6E8DB;
        }

        .header {
            padding: 20px;
            width: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1;
        }

        .container {
            padding: 100px 0px
        }

        table thead {
            background: lightblue;
        }

        .footer {
            padding: 20px;
            text-align: center;
            width: 100%;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .alert {
            width: 50%;
            margin: auto
        }

        .col-sm-6 img {
            width: 100px;
            height: auto
        }
    </style>
</head>

<body>
    @include('MasterLayout.header')

    <div class="container">
        @if (session()->has('success'))
            <div id="alert" class="alert alert-success my-2">
                {{ session()->get('success') }}
            </div>
        @endif

        @if (session()->has('delete'))
            <div id="alert" class="alert alert-warning my-2">
                {{ session()->get('delete') }}
            </div>
        @endif

        @yield('main.container')
    </div>

    @include('MasterLayout.footer')

    <script>
        setTimeout(function() {
            document.getElementById('alert').style.display = 'none';
        }, 5000); // 5000 milliseconds = 5 seconds
    </script>

</body>

</html>
