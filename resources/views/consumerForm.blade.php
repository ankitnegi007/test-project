@extends('MasterLayout.main')

@section('title', 'Home Page')

@section('main.container')
    <div class="row">
        <div class="col-sm-6 mx-auto" style="border:2px solid cyan; padding:30px">
            <h5 class="text-center">Add Consumer Form</h5>
            <form action="{{ URL::to('/') }}/consumerRegistration" method="POST">
                {!! csrf_field() !!}
                <div class="form-group mt-4">
                    <label for="name">Name:</label>
                    <input type="text" id="name" name="name"
                        class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"
                        autocomplete="off">
                    <span class="text-danger">
                        @error('name')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" value="M"
                            {{ old('gender') === 'male' ? 'checked' : '' }}>
                        <label class="form-check-label" for="male">
                            Male
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" value="F"
                            {{ old('gender') === 'female' ? 'checked' : '' }}>
                        <label class="form-check-label" for="female">
                            Female
                        </label>
                    </div>
                    <span class="text-danger">
                        @error('gender')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="email">Email:</label>
                    <input type="text" id="email" name="email"
                        class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                        autocomplete="off">
                    <span class="text-danger">
                        @error('email')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="phone">Phone:</label>
                    <input type="phone" id="phone" name="phone" maxlength="10"
                        class="form-control @error('phone') is-invalid @enderror" {{ old('phone') }} autocomplete="off">
                    <span class="text-danger">
                        @error('phone')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="state">State:</label>
                    <select class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                        <option value="">---Select State---</option>
                        <option value="Uttar Pradesh" {{ old('state') == 'Uttar Pradesh' ? 'selected' : '' }}>Uttar
                            Pradesh</option>
                        <option value="Delhi" {{ old('state') == 'Delhi' ? 'selected' : '' }}>Delhi</option>
                        <option value="Maharashtra" {{ old('state') == 'Maharashtra' ? 'selected' : '' }}>
                            Maharashtra</option>
                        <option value="Bihar" {{ old('state') == 'Bihar' ? 'selected' : '' }}>Bihar</option>
                        <option value="West Bengal" {{ old('state') == 'West Bengal' ? 'selected' : '' }}>West
                            Bengal</option>
                        <option value="Himachal Pradesh" {{ old('state') == 'Himachal Pradesh' ? 'selected' : '' }}>
                            Himachal Pradesh</option>
                        <option value="Uttarakhand" {{ old('state') == 'Uttarakand' ? 'selected' : '' }}>
                            Uttarakhand</option>
                    </select>
                    <span class="text-danger">
                        @error('state')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <button type="submit" class="btn btn-primary mt-4">Submit</button>
            </form>
        </div>
    </div>
@endsection
