@extends('MasterLayout.main')

@section('title', 'All Users Page')

@section('main.container')
    <div class="row">
        <div class="col-10"></div>
        <div class="col-2 text-end">
            <a href="{{ route('exportConsumersPdf') }}" class="p-2" style="color: #D24545"><i class="fa-solid fa-file-pdf" style="font-size: 30px"></i></a>
            <a href="{{ route('exportConsumersExcel') }}" class="p-2" style="color: #416D19"><i
                    class="fa-solid fa-file-excel" style="font-size: 30px"></i></a>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-sm-12 mx-auto">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead class="text-center">
                        <tr>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>State</th>
                            <th>Created at</th>
                            <th>Action</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $serial = 1 @endphp
                        @foreach ($data as $user)
                            <tr>
                                <td>{{ $serial++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->gender == 'M' ? 'Male' : 'Female' }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->state }}</td>
                                <td>{{ date('d-m-Y | H:i:s', strtotime($user->created_at)) }}</td>
                                <td>
                                    <form id="deleteConsumer{{ $user->id }}"
                                        action="{{ route('deleteConsumer', $user->id) }}" method="POST">
                                        @csrf
                                        <button type="button" class="btn btn-danger"
                                            onclick="confirmDelete('{{ $user->id }}')">Delete</button>
                                    </form>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary"
                                        onclick="viewDetails('{{ $user->id }}', '{{ $user->name }}', '{{ $user->gender }}', '{{ $user->email }}', '{{ $user->phone }}', '{{ $user->state }}')">View</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <script>
                    function confirmDelete(userId) {
                        if (confirm("Are you sure you want to delete this consumer?")) {
                            document.getElementById('deleteConsumer' + userId).submit();
                        }
                    }

                    function viewDetails(id, name, gender, email, phone, state) {
                        alert("User ID: " + id + "Name: " + name + "\nGender: " + gender + "\nEmail: " + email + "\nPhone: " + phone +
                            "\nState: " + state);
                    }
                </script>
            </div>
        </div>
    </div>
@endsection
