@extends('MasterLayout.main')

@section('title', 'Home Page')

@section('main.container')
    <div class="row">
        <div class="col-sm-6 mx-auto" style="border:2px solid #8cd5a0; padding:30px">
            <h5 class="text-center" style="color: green">User Registration</h5>
            <form action="{{ URL::to('/') }}/register-user" method="POST">
                {!! csrf_field() !!}
                <div class="form-group mt-4">
                    <label for="user_name">Name:</label>
                    <input type="text" id="name" name="user_name"
                        class="form-control @error('user_vname') is-invalid @enderror" value="{{ old('user_name') }}"
                        autocomplete="off">
                    <span class="text-danger">
                        @error('user_name')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="user_type" value="S"
                            {{ old('user_type') === 'supplier' ? 'checked' : '' }}>
                        <label class="form-check-label" for="supplier">
                            Supplier
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="user_type" value="C"
                            {{ old('user_type') === 'consumer' ? 'checked' : '' }}>
                        <label class="form-check-label" for="consumer">
                            Consumer
                        </label>
                    </div>
                    <span class="text-danger">
                        @error('user_type')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="user_email">Email:</label>
                    <input type="text" id="email" name="user_email"
                        class="form-control @error('user_email') is-invalid @enderror" value="{{ old('user_email') }}"
                        autocomplete="off">
                    <span class="text-danger">
                        @error('user_email')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="user_password">Password:</label>
                    <input type="password" id="password" name="user_password" minlength="8"
                        class="form-control @error('user_password') is-invalid @enderror" {{ old('user_password') }} autocomplete="off">
                    <span class="text-danger">
                        @error('user_password')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="user_password_confirmation">Password:</label>
                    <input type="password" id="password" name="user_password_confirmation" minlength="8"
                        class="form-control @error('user_password_confirmation') is-invalid @enderror" {{ old('user_password_confirmation') }} autocomplete="off">
                    <span class="text-danger">
                        @error('user_password_confirmation')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <button type="submit" class="btn btn-success mt-4">Sign Up</button>
            </form>
        </div>
    </div>
@endsection
