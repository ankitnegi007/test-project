@extends('MasterLayout.main')

@section('title', 'Home Page')

@section('main.container')
    <div class="row">
        <div class="col-sm-6 mx-auto" style="border:2px solid #8cd5a0; padding:30px">
            <h5 class="text-center">Login</h5>
            <form action="{{ URL::to('/') }}/loginUser" method="POST">
                {!! csrf_field() !!}
                
                <div class="form-group mt-4">
                    <label for="user_email">Email:</label>
                    <input type="text" id="email" name="user_email"
                        class="form-control @error('user_email') is-invalid @enderror" value="{{ old('user_email') }}"
                        autocomplete="off">
                    <span class="text-danger">
                        @error('user_email')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <div class="form-group mt-4">
                    <label for="user_password">Password:</label>
                    <input type="password" id="password" name="user_password" minlength="8"
                        class="form-control @error('user_password') is-invalid @enderror" {{ old('user_password') }} autocomplete="off">
                    <span class="text-danger">
                        @error('user_password')
                            {{ $message }}
                        @enderror
                    </span>
                </div>

                <button type="submit" class="btn btn-success mt-4">Login</button>
            </form>
        </div>
    </div>
@endsection
