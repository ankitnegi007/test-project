<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserRegistration;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', function () {
    if (session()->has('user_id')) {
        return redirect()->route('addConsumer');
    }

    return view('register');
});

Route::post('/register-user', [UserRegistration::class, 'registerUser']);

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');

Route::post('/loginUser', [LoginController::class, 'loginUser']);

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware('authenticate')->group(function () {
    
    Route::view('/consumerForm', 'consumerForm');

    Route::post('/consumerRegistration', [UserRegistration::class, 'addConsumer']);

    Route::get('/consumers', [UserRegistration::class, 'showConsumers'])->name('addConsumer');

    Route::post('/deleteConsumer/{id}', [UserRegistration::class, 'deleteConsumer'])->name('deleteConsumer');

    Route::get('/viewConsumer/{id}', [UserRegistration::class, 'viewConsumer'])->name('viewConsumer');

    Route::get('/export-consumers-excel', [UserRegistration::class, 'exportExcel'])->name('exportConsumersExcel');

    Route::get('/export-consumers-pdf', [UserRegistration::class, 'exportPdf'])->name('exportConsumersPdf');

});

// Route:: view('home', '/home');

// Route:: get("home", [userController:: class, 'show']);

// Route::get('home', 'userController@show');  //old version call

// Route:: get("/about", SingleActionController:: class); // single action controller - having only one function {invoke}

// Route:: view('about', '/about');
// Route:: view('contact', '/contact');
